package bitmax.com.br.contas;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Conta {
    @PrimaryKey(autoGenerate = true)
    private Long uid;

    @ColumnInfo
    private String nome;

    @ColumnInfo
    private Double valor;

    @ColumnInfo
    private Long vencimento;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Long getVencimento() {
        return vencimento;
    }

    public void setVencimento(Long vencimento) {
        this.vencimento = vencimento;
    }
}